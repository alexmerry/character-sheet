"""Setuptools script for the character sheet python module."""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

version_vars = {}
try:
    execfile(path.join(here, 'dndcharsheet', 'version.py'), version_vars)
except NameError:
    with open(path.join(here, 'dndcharsheet', 'version.py'), encoding='utf-8') as f:
        exec(f.read(), version_vars)

setup(
    name='dndcharsheet',
    version=version_vars['__version__'],
    description='Generates a D&D 5th Edition character sheet',
    long_description=long_description,  # Optional
    long_description_content_type='text/markdown',
    url='https://gitlab.com/alexmerry/character-sheet',
    author='Alex Merry',
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Other Audience',
        'Topic :: Artistic Software',
        'License :: OSI Approved :: MIT License',
        # no guarantees for now:
        #'Programming Language :: Python :: 2',
        #'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        #'Programming Language :: Python :: 3.4',
        #'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],

    keywords='dnd dungeons dragons 5e rpg character pdf',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    install_requires=[
        'commonmark',
        'jinja2',
        'pytoml',
        'reportlab',
        'svglib',
    ],
    package_data={
        '': ['images/*.svg'],
    },
    entry_points={
        'console_scripts': [
            'gencharsheet=dndcharsheet:main',
        ],
    },
)
