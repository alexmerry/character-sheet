import collections

from . import util

from reportlab.lib.enums import TA_CENTER
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import mm
from reportlab.platypus import Flowable, Paragraph

NORMAL_FONT = "Helvetica"
BOLD_FONT = "Helvetica-Bold"

DEFAULT_SPACING = 0
MIN_CIRCLE_RADIUS = mm # you can't realistically pencil in anything smaller
MAX_CIRCLE_RADIUS = 1.5 * mm # starts to look silly after this
MIN_FONTSIZE = 7

def circle_line_height(radius):
    return radius * 3

MIN_WIDTH = circle_line_height(MIN_CIRCLE_RADIUS) * (util.GOLDEN_RATIO * 8)

def small_line_height(width):
    """Get the height of a smaller line of text based on an available width.

    This is tuned to make SkillBox look good (ie: have a golden ratio between width and height)
    based on SkillBox.MAXSKILLS being 5.
    """
    return max(width / (util.GOLDEN_RATIO * 8), circle_line_height(MIN_CIRCLE_RADIUS))

def normal_line_height(width):
    """Get the height of a normal stats box based on an available width.
    """
    return 1.4 * small_line_height(width)

def header_height(width):
    """Get the height of the header of a complex box based on an available width.

    This is tuned to make SkillBox look good (ie: have a golden ratio between width and height)
    based on SkillBox.MAXSKILLS being 5.
    """
    return 1.6 * small_line_height(width)

def statsblock_header_height(width):
    """Get the height of the header of a complex box based on an available width.

    This is tuned to make SkillBox look good (ie: have a golden ratio between width and height)
    based on SkillBox.MAXSKILLS being 5.
    """
    return 2 * small_line_height(width)

class TextAligner(object):
    def __init__(self, x, y, width, height, fontsize=None, padding=0):
        self.padding = padding
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.fontsize = fontsize or (height * 2/3)
        self.halign = 'LEFT'
        self.valign = 'MIDDLE'

    def render(self, canvas, text, font = NORMAL_FONT):
        canvas.setFont(font, self.fontsize)

        if self.halign == 'LEFT':
            x = self.x + self.padding
            draw = canvas.drawString
        elif self.halign == 'CENTER' or self.halign == 'CENTRE':
            x = self.x + self.width/2
            draw = canvas.drawCentredString
        elif self.halign == 'RIGHT':
            x = self.x + self.width - self.padding
            draw = canvas.drawRightString
        else:
            raise RuntimeError("Unknown halign value \"{}\"".format(self.halign))

        if self.valign == 'BOTTOM':
            y = self.y + self.padding + self.fontsize/4
        elif self.valign == 'MIDDLE':
            # for some reason, it looks better if you essentially ignore the tails
            y = self.y + self.height/2 - self.fontsize * 0.375
        elif self.valign == 'TOP':
            y = self.y + self.height - self.padding - self.fontsize * 0.75
        else:
            raise RuntimeError("Unknown valign value \"{}\"".format(self.valign))

        draw(x, y, text)

def textbase(fontsize, lineheight):
    return fontsize * 0.35

def textbase_allcaps(fontsize, lineheight):
    # A capital letter extends from 0 to (fontsize*0.75) above the y co-ord where it is written.
    # This aims to place the middle of the letter at (lineheight/2)
    return (lineheight/2) - fontsize * 0.375

class _Header(object):
    def __init__(self, text, textsize=None):
        self.text = text
        self.textsize = textsize
        self.width = None
        self.height = 0
        self.style = ParagraphStyle("default")
        self.style.alignment = TA_CENTER

    def wrap(self, availWidth, availHeight):
        self.width = availWidth

        if self.text:
            if self.textsize:
                self.style.fontSize = self.textsize
            else:
                self.style.fontSize = header_height(self.width) * 2/3
            self.style.leading = self.style.fontSize * 1.2
            # Paragraph style isn't updated properly after it is constructed
            self.para = Paragraph(self.text, self.style)
            (w, self.height) = self.para.wrap(self.width, availHeight)
            self.height += self.style.fontSize * 0.2
        else:
            self.height = 0

    def draw(self, canv, x=0, y=0):
        if not self.text:
            return

        basepadding = self.style.fontSize * 0.2
        self.para.drawOn(canv, 0, y + basepadding)
        canv.line(0, y, self.width, y)

class ScoreBox(Flowable):
    def __init__(self, name, value=None, icon=None, is_mod=False):
        self.name = name
        if value is None:
            self.value = None
        else:
            if is_mod:
                self.value = "{:+}".format(value)
            else:
                self.value = str(value)
        self.icon = icon
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING
        self.width = None

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def height(self):
        return normal_line_height(self.width or 0)

    def wrap(self, availWidth, availHeight):
        """Flowable overload."""

        self.width = max(min(availWidth, self.width or availWidth), MIN_WIDTH)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    def draw(self):
        """Flowable overload."""
        self.canv.roundRect(0, 0, self.width, self.height, radius=min(4, self.height / 3))
        scoreboxw = self.height * util.GOLDEN_RATIO
        scoreboxl = self.width - scoreboxw
        self.canv.line(scoreboxl, 0, scoreboxl, self.height)

        aligner = TextAligner(0, 0, self.width - scoreboxw, self.height, padding=self.height/5)
        aligner.render(self.canv, self.name)

        if self.value is not None:
            aligner.x = scoreboxl
            aligner.width = scoreboxw
            aligner.halign = 'CENTER'
            aligner.render(self.canv, str(self.value))

        if self.icon is not None:
            drawing = util.scaledsvg(self.icon, height=self.height * 0.8)
            innermargin = self.height * 0.1
            drawing.drawOn(self.canv, scoreboxl - drawing.width - innermargin, innermargin)

class StatsBlock(Flowable):
    MAXSKILLS = 5
    DEFAULT_WIDTH = 150
    NAME = None
    SKILLS = []
    ICON = None

    def __init__(self, name=None, score=None, skills=None, icon=None, showexpertise=False,
                 savingthrowproficient=False, proficiencymod=None, savingthrowbonus=0):
        self.name = name or self.NAME
        self.savingthrowproficient = savingthrowproficient
        self.skills = {s: util.SkillLevel.Normal for s in self.SKILLS}
        self.skills.update(skills)
        self.score = score
        self.showexpertise = showexpertise
        self.proficiencymod = proficiencymod
        self.savingthrowbonus = savingthrowbonus
        self.icon = icon or self.ICON
        self.width = None
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING

    def _ensureDims(self, def_width):
        if self.height is None:
            if self.width is None:
                self.width = def_width
            self.height = self.width / util.GOLDEN_RATIO
        elif self.width is None:
            self.width = self.height * util.GOLDEN_RATIO

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def _skill_height(self):
        return small_line_height(self.width or 0)

    @property
    def _header_height(self):
        return statsblock_header_height(self.width or 0)

    @property
    def _skillbox_padding(self):
        return self._skill_height / 2

    @property
    def _skillbox_height(self):
        return (StatsBlock.MAXSKILLS * self._skill_height) + (2 * self._skillbox_padding)

    @property
    def height(self):
        return self._header_height + self._skillbox_height

    @property
    def _circle_radius(self):
        return min(self._skill_height / 3, MAX_CIRCLE_RADIUS)


    def wrap(self, availWidth, availHeight):
        if self.width is None:
            self.width = availWidth
        else:
            self.width = min(self.width, availWidth)

        self.width = max(self.width, MIN_WIDTH)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    @property
    def shortname(self):
        return self.name[:3].upper()

    @property
    def mod(self):
        if self.score is None:
            return None
        return util.score_to_mod(self.score)

    @property
    def savingthrowmod(self):
        if self.score is None or self.proficiencymod is None:
            return None
        mod = self.mod + self.savingthrowbonus
        if self.savingthrowproficient:
            mod += self.proficiencymod
        return mod

    def _leveltomod(self, level):
        return self.mod + self.proficiencymod * level.value

    def skillmod(self, skill):
        if self.score is None or self.proficiencymod is None:
            return None
        return self._leveltomod(self.skills[skill])

    def skilltext(self, skill):
        return skill

    def drawContents(self, width, height):
        if not self.skills:
            return

        skillcount = len(self.skills)
        missing = self.MAXSKILLS - skillcount

        skillheight = self._skill_height
        skillwidth = skillheight * util.GOLDEN_RATIO
        linemid = skillheight / 2
        r = self._circle_radius
        textbase = linemid - r

        # we'll just translate the canvas as appropriate
        label_aligner = TextAligner(0, 0, width, skillheight)
        score_aligner = TextAligner(0, 0, skillwidth, skillheight)
        score_aligner.halign = "CENTER"

        self.canv.roundRect(0, missing*skillheight, skillwidth, skillheight*skillcount, radius=1)
        self.canv.lines((0, y*skillheight, skillwidth, y*skillheight)
                        for y in range(missing+1, self.MAXSKILLS))
        for i, (skill, level) in enumerate(reversed(list(self.skills.items())), start=missing):
            with util.savedstate(self.canv):
                self.canv.translate(0, i*skillheight)
                if self.score is not None and self.proficiencymod is not None:
                    score_aligner.render(self.canv, "{:+}".format(self._leveltomod(level)))
                self.canv.translate(skillwidth, 0)
                for i in range(1, 3 if self.showexpertise else 2):
                    self.canv.setFillColorRGB(0.6, 0.6, 0.6)
                    self.canv.line(0, linemid, r, linemid)
                    self.canv.circle(2 * r, linemid, r, fill=(level.value >= i))
                    self.canv.translate(3*r, 0)
                self.canv.translate(r, 0)
                self.canv.setFillColorRGB(0, 0, 0)
                label_aligner.render(self.canv, self.skilltext(skill))

    def draw(self):
        """Flowable overload. Use drawOn() instead."""
        headerheight = self._header_height
        headerbase = self.height - headerheight
        self.canv.roundRect(0, 0, self.width, self.height, radius=4)
        self.canv.line(0, headerbase, self.width, headerbase)
        headerdivs = [(x*self.width, self.height, x*self.width, headerbase) for x in [0.25, 0.5, 0.75]]
        self.canv.lines(headerdivs)

        with util.savedstate(self.canv):
            if self.savingthrowproficient:
                self.canv.setFillColorRGB(0.6, 0.6, 0.6)
            else:
                self.canv.setFillColorRGB(1, 1, 1)
            self.canv.circle(self.width * 0.875, headerbase, self._circle_radius, fill=True)

        headerbox_w = self.width/4
        aligner = TextAligner(0, headerbase, headerbox_w, headerheight)
        aligner.halign = 'CENTER'
        aligner.render(self.canv, self.shortname)

        fontsize = headerheight * 2/3
        headermid = headerbase + headerheight/2
        linebase = headerbase + textbase_allcaps(fontsize, headerheight)

        with util.savedstate(self.canv):
            fontsize = headerheight * 0.3
            margin = fontsize/4
            self.canv.setFontSize(fontsize)
            self.canv.setFillColorRGB(0.6, 0.6, 0.6)
            self.canv.rotate(90)
            self.canv.drawCentredString(headermid, margin-self.width*0.5, "Score")
            self.canv.drawCentredString(headermid, margin-self.width*0.75, "Mod")
            self.canv.drawCentredString(headermid, margin-self.width, "Save")

        if self.score is not None:
            aligner.width -= headerheight * 0.15 # shift text left for box titles
            aligner.x += headerbox_w
            aligner.render(self.canv, str(self.score))
            aligner.x += headerbox_w
            aligner.render(self.canv, "{:+}".format(self.mod), font=BOLD_FONT)
            aligner.x += headerbox_w
            aligner.render(self.canv, "{:+}".format(self.savingthrowmod))

        padding = self._skillbox_padding

        if self.icon is not None:
            drawing = util.scaledsvg(self.icon, height=headerbase * 0.6)
            drawing.drawOn(self.canv, self.width - drawing.width - padding,
                           headerbase/2 - drawing.height/2)

        with util.savedstate(self.canv):
            self.canv.translate(padding, padding)
            self.drawContents(self.width - 2*padding, headerbase - 2*padding)


class StrengthBlock(StatsBlock):
    NAME = "Strength"
    SKILLS = ["Athletics"]
    ICON = "weight-lifting-up"

class DexterityBlock(StatsBlock):
    NAME = "Dexterity"
    SKILLS = ["Acrobatics", "Sleight of Hand", "Stealth"]
    ICON = "high-kick"

class ConstitutionBlock(StatsBlock):
    NAME = "Constitution"
    ICON = "half-heart"

class IntelligenceBlock(StatsBlock):
    NAME = "Intelligence"
    SKILLS = ["Arcana", "History", "Investigation", "Nature", "Religion"]
    ICON = "enlightenment"

class WisdomBlock(StatsBlock):
    NAME = "Wisdom"
    SKILLS = ["Animal Handling", "Insight", "Medicine", "Perception", "Survival"]
    ICON = "owl"

    def skilltext(self, skill):
        if util.normalise(skill) == "perception":
            return "{} ({} passive)".format(skill, 10 + self.skillmod(skill))
        return skill

class CharismaBlock(StatsBlock):
    NAME = "Charisma"
    SKILLS = ["Deception", "Intimidation", "Performance", "Persuasion"]
    ICON = "fedora"


class SpellBlock(Flowable):
    def __init__(self, title="Spellcasting", modifier=None, save_dc=None, spell_slots=[], titlesize=None):
        self.modifier = modifier
        self.save_dc = save_dc
        self.spell_slots = list(spell_slots) # make sure we have our own list instance
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING
        self.width = None

        self.header = _Header(title, titlesize)

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def height(self):
        slot_entries = len([x for x in self.spell_slots if x != 0])
        if slot_entries > 0:
            slots_area_height = (slot_entries + 1) * small_line_height(self.width)
        else:
            slots_area_height = 0
        return (self.header.height +
                2 * normal_line_height(self.width) +
                slots_area_height)

    def wrap(self, availWidth, availHeight):
        """Flowable overload."""

        self.width = max(min(availWidth, self.width or availWidth), MIN_WIDTH)

        self.header.wrap(self.width, availHeight)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    def _draw_score_line_icon(self, icon, x_right, y, height):
        drawing = util.scaledsvg(icon, height=height * 0.8)
        innermargin = height * 0.1
        drawing.drawOn(self.canv, x_right - drawing.width - innermargin, y + innermargin)

    def draw(self):
        """Flowable overload."""
        self.canv.roundRect(0, 0, self.width, self.height, radius=min(4, self.height / 3))

        # header
        h = self.header.height
        y = self.height - h
        self.header.draw(self.canv, y=y)

        # score box lines
        h = normal_line_height(self.width)
        scoreboxw = h * util.GOLDEN_RATIO
        scoreboxl = self.width - scoreboxw
        scoreboxmid = scoreboxl + scoreboxw/2
        padding = h/5

        label_aligner = TextAligner(0, y, scoreboxl, h, padding=padding)
        score_aligner = TextAligner(scoreboxl, y, scoreboxw, h)
        score_aligner.halign = 'CENTER'

        self.canv.line(scoreboxl, y, scoreboxl, y - 2 * h)

        ## mod
        self.canv.line(0, y, self.width, y)
        y -= h
        label_aligner.y = y
        label_aligner.render(self.canv, "Attack Bonus")
        if self.modifier is not None:
            score_aligner.y = y
            score_aligner.render(self.canv, "{:+}".format(self.modifier))

        ## score
        self.canv.line(0, y, self.width, y)
        y -= h
        label_aligner.y = y
        label_aligner.render(self.canv, "Save DC")
        if self.save_dc is not None:
            score_aligner.y = y
            score_aligner.render(self.canv, str(self.save_dc))

        slot_entry_lines = len([x for x in self.spell_slots if x != 0])
        if slot_entry_lines > 0:
            self.canv.line(0, y, self.width, y)

        h = small_line_height(self.width)
        r = min(h / 3, MAX_CIRCLE_RADIUS)
        y -= h/2

        aligner = TextAligner(self.width/2, y, self.width/2, h, padding=padding)

        for level, slots in enumerate(self.spell_slots, start=1):
            if slots == 0:
                continue
            y -= h
            aligner.y = y
            aligner.render(self.canv, "Level {} slots".format(level))
            mid_y = y + h/2
            for i in range(slots):
                x = self.width/2 - i * r * 3
                if i != 0:
                    self.canv.line(x, mid_y, x - r, mid_y)
                self.canv.circle(x - 2 * r, mid_y, r)

class Counters(Flowable):
    def __init__(self, counters=[], title=None, titlesize=None, show_zeroes=False):
        if isinstance(counters, collections.Mapping):
            self.counters = sorted(counters.items())
        else:
            self.counters = counters
        self.show_zeroes = show_zeroes
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING
        self.width = None

        self.header = _Header(title, titlesize)

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def visiblecounters(self):
        if self.show_zeroes:
            return self.counters
        else:
            return [(x,y) for (x,y) in self.counters if y != 0]

    @property
    def height(self):
        entry_count = len(self.visiblecounters)
        slots_area_height = (entry_count + 1) * small_line_height(self.width)
        return self.header.height + slots_area_height

    def wrap(self, availWidth, availHeight):
        """Flowable overload."""

        self.width = max(min(availWidth, self.width or availWidth), MIN_WIDTH)

        self.header.wrap(self.width, availHeight)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    def draw(self):
        """Flowable overload."""
        self.canv.roundRect(0, 0, self.width, self.height, radius=min(4, self.height / 3))

        # header
        h = self.header.height
        y = self.height - h

        self.header.draw(self.canv, y=y)

        h = small_line_height(self.width)
        r = min(h / 3, MAX_CIRCLE_RADIUS)
        y -= h/2

        aligner = TextAligner(self.width/2, y, self.width/2, h, padding=h/5)

        for name, count in self.visiblecounters:
            y -= h
            aligner.y = y
            aligner.render(self.canv, name)
            mid_y = y + h/2
            for i in range(count):
                x = self.width/2 - i * r * 3
                if i != 0:
                    self.canv.line(x, mid_y, x - r, mid_y)
                self.canv.circle(x - 2 * r, mid_y, r)

class Counter(Flowable):
    def __init__(self, count=1, title=None, titlesize=None, style="dots"):
        self.count = count
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING
        self.width = None
        self.style = style

        self.header = _Header(title, titlesize)

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def height(self):
        if self.style == "dots":
            lines = self.count // self.dotsperline
            if self.count % self.dotsperline:
                lines += 1
            slots_area_height = (lines + 1) * small_line_height(self.width)
            return self.header.height + slots_area_height
        else:
            return self.header.height + 3 * small_line_height(self.width)

    @property
    def dotsperline(self):
        r = min(small_line_height(self.width) / 3, MAX_CIRCLE_RADIUS)
        return int((self.width - 2 * r) // (3 * r))

    def wrap(self, availWidth, availHeight):
        """Flowable overload."""

        self.width = max(min(availWidth, self.width or availWidth), MIN_WIDTH)

        self.header.wrap(self.width, availHeight)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    def draw(self):
        """Flowable overload."""
        self.canv.roundRect(0, 0, self.width, self.height, radius=min(4, self.height / 3))

        # header
        h = self.header.height
        y = self.height - h

        self.header.draw(self.canv, y=y)

        if self.style == "dots":
            h = small_line_height(self.width)
            r = min(h / 3, MAX_CIRCLE_RADIUS)
            y -= h/2

            line_count = self.dotsperline
            full_lines = int(self.count // line_count)
            last_line = self.count % line_count
            if full_lines:
                line_width = (3 * r * line_count) - r
            else:
                line_width = (3 * r * last_line) - r

            line_left = (self.width - line_width) / 2

            def draw_line(count, y):
                mid_y = y + h/2
                x = line_left
                for i in range(count):
                    self.canv.circle(x + r, mid_y, r)
                    x += 2 * r
                    if i + 1 < count:
                        self.canv.line(x, mid_y, x + r, mid_y)
                        x += r

            for i in range(full_lines):
                y -= h
                draw_line(line_count, y)

            if last_line:
                y -= h
                draw_line(last_line, y)
        else:
            self.canv.setFillColorRGB(0.8, 0.8, 0.8)
            self.canv.setFont(BOLD_FONT, y * 0.8)
            self.canv.drawCentredString(self.width/2, y * 0.2, str(self.count))

class HitPointsArea(Flowable):
    def __init__(self, maxhp=None, title=None, hidetitle=False, hidemax=False, titlesize=None):
        if hidetitle:
            title = None
        elif title is None:
            if maxhp:
                title = "Hit Points (max {})".format(maxhp)
            else:
                title = "Hit Points"
        if hidemax:
            self.maxhp = None
        else:
            self.maxhp = maxhp
        self.hAlign = 'CENTRE'
        self.spacing = DEFAULT_SPACING
        self.width = None
        self.header = _Header(title, titlesize)

    def getSpaceBefore(self):
        return self.spacing

    def getSpaceAfter(self):
        return self.spacing

    @property
    def height(self):
        return self.header.height + 3 * small_line_height(self.width)

    def wrap(self, availWidth, availHeight):
        """Flowable overload."""

        self.width = max(min(availWidth, self.width or availWidth), MIN_WIDTH)

        self.header.wrap(self.width, availHeight)

        return (self.width, self.height)

    def split(self, availWidth, availHeight):
        """Flowable overload. Returns [] because we can't split."""
        return []

    def draw(self):
        """Flowable overload."""
        self.canv.roundRect(0, 0, self.width, self.height, radius=min(4, self.height / 3))

        # header
        h = self.header.height
        y = self.height - h

        self.header.draw(self.canv, y=y)

        div = self.width * 2/3
        self.canv.line(div, 0, div, y)

        if self.maxhp:
            with util.savedstate(self.canv):
                self.canv.setFillColorRGB(0.8, 0.8, 0.8)
                self.canv.setFont(BOLD_FONT, y * 0.8)
                self.canv.drawCentredString(div/2, y * 0.2, str(self.maxhp))

        with util.savedstate(self.canv):
            self.canv.setFillColorRGB(0.8, 0.8, 0.8)
            self.canv.setFont(NORMAL_FONT, y * 0.4)
            self.canv.drawCentredString(div * 1.25, y * 0.4, "TEMP")
