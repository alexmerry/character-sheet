#!/usr/bin/env python

import argparse
import collections
import itertools
import jinja2
import json
import pytoml as toml
import os.path

from reportlab.pdfgen import canvas
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.platypus import Frame, Paragraph

from . import components, util
from .components import *
from .util import SkillLevel, normalise, normalise_keys

Margins = collections.namedtuple("Margins", ("left", "bottom", "right", "top"))
PAGE_MARGINS =  Margins(18, 18, 18, 18)
PAGE_SIZE = tuple(reversed(A4)) # landscape
PAGE_CONTENT_SIZE = (PAGE_SIZE[0] - PAGE_MARGINS.left - PAGE_MARGINS.right,
                     PAGE_SIZE[1] - PAGE_MARGINS.bottom - PAGE_MARGINS.top)
SPACING = 6

components.DEFAULT_SPACING = SPACING

default_layout = [[
    {
        "items": [
            "@proficiency_bonus",
            "@strength",
            "@dexterity",
            "@constitution",
            "",
        ],
    },
    {
        "items": [
            "@inspiration",
            "@intelligence",
            "@wisdom",
            "@charisma",
            "",
        ],
    },
    {
        "span": 2,
        "items": [
            "",
        ],
    },
    {
        "items": [
            "@ac",
            "@speed",
            "@spell_stats",
            "",
        ],
    },
]]

class BoxGenerator(object):
    def __init__(self, constructors = None):
        self.constructors = constructors or {}
        self._cmrenderer = util.CommonMarkRenderer()
        self._parseargval = json.JSONDecoder().raw_decode

    def _parseboxdesc(self, desc):
        items = desc.split(':', 1)
        orig_boxname = items[0].strip()
        boxname = normalise(orig_boxname)
        if boxname not in self.constructors:
            raise ValueError("Unknown box '{}'".format(orig_boxname))

        args = {}
        if len(items) > 1:
            argstr = items[1]
        else:
            argstr = None

        while argstr:
            try:
                argname, argstr = argstr.split('=', 1)
            except ValueError:
                raise ValueError("Could not parse arguments for '{}': could not find '=' in '{}'".format(orig_boxname, argstr))
            if not argname:
                raise ValueError("Could not parse arguments for '{}': empty argument name".format(orig_boxname))
            if argname in args:
                raise ValueError("Could not parse arguments for '{}': duplicate argument '{}'".format(orig_boxname, argname))

            try:
                argval, end = self._parseargval(argstr)
            except json.JSONDecodeError as e:
                raise ValueError("Could not parse '{}' argument for '{}': {}".format(argname, orig_boxname, e))

            args[argname] = argval

            if len(argstr) > end and argstr[end] != ':':
                raise ValueError("Could not parse arguments for '{}': Extra data: '{}'".format(orig_boxname, argstr[end:]))

            argstr = argstr[end+1:]

        return boxname, args

    def __call__(self, desc):
        if desc.startswith('@@'):
            # the second '@' escapes the first
            return self._cmrenderer.render(desc[1:])
        elif desc.startswith('@'):
            boxname, boxargs = self._parseboxdesc(desc[1:])
            try:
                Box = self.constructors[boxname]
            except KeyError:
                raise RuntimeError("No such box type: {}".format(boxname))
            box = Box(**boxargs)
            if isinstance(box, collections.Sequence):
                return box
            else:
                return [box]
        return self._cmrenderer.render(desc)

def wrap_defaults(f, defaults):
    # shallow copy
    args = dict(defaults)
    def wrapped(**kwargs):
        args.update(kwargs)
        return f(**args)
    return wrapped

class CharacterInfo(object):
    def __init__(self, config):
        self.config = normalise_keys(config)
        self.get = self.config.get

        self.classes = self.config.setdefault("class", [])
        self.stats = self.config.setdefault("stats", {})
        self.proficiencies = self.config.setdefault("proficiencies", {})
        self.race = self.config.setdefault("race")
        self.alignment = self.config.setdefault("alignment")
        self.background = self.config.setdefault("background")
        self.proficiency_bonus = None
        self.hit_dice = None

        stats_areas = ["strength", "dexterity", "constitution", "intelligence", "wisdom", "charisma"]
        self.config["mods"] = {n: util.score_to_mod(self.stats.get(n, 10)) for n in stats_areas}

        if self.classes:

            try:
                level = max(cls["level"] for cls in self.classes)
                self.proficiency_bonus = self.stats.setdefault("proficiency_bonus", 2 + (level - 1) // 4)
            except KeyError:
                pass

            if "level" not in self.config:
                try:
                    self.config["level"] = sum(cls["level"] for cls in self.classes)
                except KeyError:
                    pass

            try:
                hit_dice_dict = {}
                for cls in self.classes:
                    dt = cls["hit_dice_type"]
                    hit_dice_dict[dt] = hit_dice_dict.get(dt, 0) + cls["level"]

                hit_dice = ' + '.join(str(hit_dice_dict[dt]) + dt for dt in sorted(hit_dice_dict))
            except KeyError:
                hit_dice = None

            self.hit_dice = self.stats.setdefault("hit_dice", hit_dice)

        self.config["proficiency_bonus"] = self.proficiency_bonus

        for cls in self.classes:
            if "spell_stat" in cls and len(self.stats):
                score = self.stats[normalise(cls["spell_stat"])]
                cls.setdefault("spell_mod", util.score_to_mod(score) + self.proficiency_bonus)
                cls.setdefault("spell_save_dc", 8 + cls["spell_mod"])

        if len(self.classes) == 1:
            classinfo = normalise_keys(self.classes[0])
            classinfo.update(self.config)
            self.config = classinfo

    def __get__(self, item):
        return self.config.get(item)

    def __iter__(self):
        return iter(self.config)

    def __len__(self):
        return len(self.config)

def build_boxes(charinfo, resolvevars, force_expertise=False):
    constructors = {}

    def register(name):
        def _register(f):
            constructors[name] = f
        return _register

    constructors["scorebox"] = ScoreBox
    constructors["counters"] = Counters
    constructors["counter"] = Counter

    @register("proficiency_bonus")
    def box(value=charinfo.proficiency_bonus):
        return ScoreBox("Proficiency Bonus", value=value, is_mod=True, icon="jump-across")

    @register("inspiration")
    def box():
        return ScoreBox("Inspiration", icon="jump-across")

    @register("ac")
    def box(value=charinfo.stats.get("ac")):
        return ScoreBox("Armour Class (AC)", value=value, icon="shoulder-armor")

    @register("hp")
    def box(maxhp=charinfo.stats.get("hp"), **kwargs):
        return HitPointsArea(maxhp=maxhp, **kwargs)

    @register("speed")
    def box(value=charinfo.stats.get("speed")):
        return ScoreBox("Speed", value=value, icon="walking-boot")

    skill_proficiencies = charinfo.proficiencies.get("skills", {})
    showexpertise = force_expertise or max(skill_proficiencies.values(), default=0) > 1

    stats = normalise_keys(charinfo.stats)
    saving_throw_profs = normalise_keys(charinfo.proficiencies.get("saving_throws", {}))
    skill_levels = {normalise(key): SkillLevel(int(value))
                    for key, value in skill_proficiencies.items()}

    for StatsBox in [StrengthBlock, DexterityBlock, ConstitutionBlock, IntelligenceBlock,
                     WisdomBlock, CharismaBlock]:
        name = normalise(StatsBox.NAME)

        constructors[name] = wrap_defaults(StatsBox, {
            "score": stats.get(name),
            "skills": {skill: skill_levels.get(normalise(skill), SkillLevel.Normal)
                       for skill in StatsBox.SKILLS},
            "proficiencymod": charinfo.proficiency_bonus,
            "showexpertise": showexpertise,
            "savingthrowproficient": saving_throw_profs.get(name, False),
            "savingthrowbonus": (int(resolvevars(stats.get("saving_throw_bonus", 0))) +
                                 int(resolvevars(stats.get(name+"_saving_throw_bonus", 0))))
        })

    @register("spell_stats")
    def box():
        spell_boxes = []

        # need one per spell-casting class...
        classes = charinfo.classes
        for cls in classes:
            if "spell_stat" not in cls and "spell_mod" not in cls and "spell_save_dc" not in cls:
                continue
            title = "Spellcasting"
            if len(classes) > 1:
                title = cls["name"] + " Spellcasting"
            block = SpellBlock(title, cls.get("spell_mod"), cls.get("spell_save_dc"),
                               cls.get("spell_slots", []))
            spell_boxes.append(block)
        return spell_boxes

    return constructors

def main():
    argparser = argparse.ArgumentParser(description="Generate a character sheet PDF")
    argparser.add_argument("config", nargs="?", type=os.path.abspath,
                           help="TOML file describing the character")
    argparser.add_argument("--output", "-o", type=os.path.abspath,
                           help="Name of the output file to produce (default based on input filename)")
    argparser.add_argument("--show-expertise", action="store_true",
                           help="Display expertise markers, even if no skills are at expert level")
    args = argparser.parse_args()

    if args.config is None:
        config = {}
        if args.output is None:
            args.output = "character-sheet.pdf"
    else:
        with open(args.config, "rb") as f:
            config = toml.load(f)
        if args.output is None:
            args.output = os.path.splitext(args.config)[0] + ".pdf"

        # all paths in the config should be relative to the config
        os.chdir(os.path.dirname(args.config))

    charinfo = CharacterInfo(config)

    def resolvevars(s):
        if isinstance(s, str):
            tmpl = jinja2.Template(s)
            return tmpl.render(charinfo.config)
        elif isinstance(s, collections.Mapping):
            return {k: resolvevars(v) for k, v in s.items()}
        elif isinstance(s, collections.Sequence):
            return [resolvevars(v) for v in s]
        else:
            return s

    boxes = BoxGenerator(build_boxes(charinfo, resolvevars, force_expertise=args.show_expertise))

    c = canvas.Canvas(args.output, pagesize=PAGE_SIZE, verbosity=1)
    c.setAuthor("Alex Merry")
    c.setTitle(charinfo.get("name", "Character Sheet"))
    c.setStrokeColorRGB(0, 0, 0)
    c.setFillColorRGB(0, 0, 0)

    pagewidth, pageheight = PAGE_CONTENT_SIZE

    pages = resolvevars(charinfo.get("pages", default_layout))

    for page in pages:
        c.translate(PAGE_MARGINS.left, PAGE_MARGINS.bottom)

        columns = page.get("columns", [])

        if page.get("header", True):
            headerheight = 50

            with util.savedstate(c):
                c.translate(SPACING/2, pageheight - headerheight + SPACING/2)
                width = pagewidth - SPACING
                height = headerheight - SPACING
                c.roundRect(0, 0, width, height, radius=4)

                if args.config:
                    h1style = ParagraphStyle("h1")
                    h1style.fontSize = height * 2/3
                    h1style.leading = h1style.fontSize * 1.2
                    h1style.alignment = TA_CENTER
                    p = Paragraph(charinfo.get("name", ""), h1style)
                    (w, h) = p.wrap(width/2, height)
                    p.drawOn(c, width/2 - w/2, height/2 - h/2)

                    h2style = ParagraphStyle("h2")
                    h2style.fontSize = height / max(4, (1 + len(charinfo.classes)))
                    h2style.leading = h2style.fontSize * 1.2
                    h2style.alignment = TA_LEFT
                    suffixes = {1: "st", 2: "nd", 3: "rd"}
                    if len(charinfo.classes) == 1:
                        cls = charinfo.classes[0]
                        class_desc = "{level}{suffix} level<br />{cls}".format(
                            level=cls["level"], cls=cls["name"], suffix=suffixes.get(cls["level"], "th"))
                    else:
                        class_desc = '<br />'.join(
                            "{level}{suffix} level {cls}".format(level=cls["level"], cls=cls["name"],
                                                                suffix=suffixes.get(cls["level"], "th"))
                            for cls in charinfo.classes
                        )
                    p = Paragraph(class_desc, h2style)
                    (w, h) = p.wrap(width/4, height)
                    p.drawOn(c, SPACING, height/2 - h/2)

                    h2style.fontSize = height / 4
                    h2style.leading = h2style.fontSize * 1.2
                    h2style.alignment = TA_RIGHT
                    summary = '<br />'.join((charinfo.race, charinfo.alignment, charinfo.background))
                    p = Paragraph(summary, h2style)
                    (w, h) = p.wrap(width/4, height)
                    p.drawOn(c, width - w - SPACING, height/2 - h/2)

            bodyheight = pageheight - headerheight
        else:
            bodyheight = pageheight

        colcount = sum(c.get("span", 1) for c in columns)
        colwidth = pagewidth / colcount

        left = 0
        for col in columns:
            span = col.get("span", 1)
            frame = Frame(left, 0, colwidth * span, bodyheight, leftPadding=SPACING/2,
                        bottomPadding=SPACING/2, rightPadding=SPACING/2, topPadding=SPACING/2)
            left += colwidth * span
            frame.addFromList(list(itertools.chain.from_iterable(map(boxes, col["items"]))), c)

        c.showPage()

    c.save()

if __name__ == "__main__":
    main()
