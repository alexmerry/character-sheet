import CommonMark
import io
import os
import pkgutil
import svglib.svglib as svglib

import lxml.etree as etree # svglib needs the lxml version of etree

from contextlib import contextmanager
from enum import Enum
from reportlab.platypus import Paragraph, ListFlowable

GOLDEN_RATIO = 1.618

class SkillLevel(Enum):
    Normal = 0
    Proficient = 1
    Expert = 2

def normalise(key):
    return key.lower().replace(" ", "_")

def normalise_keys(d):
    return {normalise(key): value for key, value in d.items()}

def score_to_mod(score):
    return (score - 10) // 2

@contextmanager
def savedstate(canvas):
    canvas.saveState()
    try:
        yield canvas
    finally:
        canvas.restoreState()

_xml_parser = etree.XMLParser()#remove_comments=True, recover=True)

def _tosvg(fileobj):
    doc = etree.parse(fileobj, parser=_xml_parser)
    return svglib.SvgRenderer().render(doc.getroot())

def scaledsvg(path, width=None, height=None):
    if os.path.exists(path):
        with open(path, "rb") as f:
            drawing = _tosvg(f)
    else:
        data = pkgutil.get_data(__name__, "images/" + path + ".svg")
        if data is not None:
            drawing = _tosvg(io.BytesIO(data))
        else:
            raise RuntimeError("Cannot find {}".format(path))

    if drawing is None:
        raise RuntimeError("Failed to parse {}".format(path))

    if (width, height) == (None, None):
        return drawing
    elif width is None:
        yscale = height / drawing.height
        xscale = yscale
        width = drawing.width * xscale
    elif height is None:
        xscale = width / drawing.width
        yscale = xscale
        height = drawing.height * yscale
    else:
        xscale = width / drawing.width
        yscale = height / drawing.height

    drawing.scale(xscale, yscale)
    drawing.width = width
    drawing.height = height

    return drawing


class CommonMarkRenderer(object):
    class AstSeq(object):
        def __init__(self, parent):
            self.node = parent.first_child

        def __iter__(self):
            return self

        def __next__(self):
            node = self.node
            if node is None:
                raise StopIteration()
            self.node = node.nxt
            return node

    def __init__(self, stylesheet=None):
        self.cmparser = CommonMark.Parser()
        self.cmtohtml = CommonMark.HtmlRenderer().render

        if stylesheet is not None:
            self.styles = stylesheet
        else:
            from reportlab.lib.styles import getSampleStyleSheet
            self.styles = getSampleStyleSheet()
            self.styles["ul"].bulletOffsetY = 1.5
            self.styles["ul"].leftIndent = 8

    def cmnode_to_flowable(self, node, pstyle=None):
        if node.t == "paragraph":
            return Paragraph(self.cmtohtml(node), pstyle or self.styles["BodyText"])
        elif node.t == "list":
            if node.list_data["type"] == "bullet":
                style=self.styles["ul"]
                bulletType = "bullet"
                start = None
            else:
                style=self.styles["ol"]
                bulletType = "1"
                start = node.list_data.start
            return ListFlowable([self.cmnode_to_flowable(n, pstyle) for n in self.AstSeq(node)],
                                bulletType=bulletType,
                                style=style,
                                start=start)
        elif node.t == "item":
            children = []
            childnode = node.first_child
            while childnode:
                children.append(self.cmnode_to_flowable(childnode, pstyle or self.styles["Normal"]))
                childnode = childnode.nxt
            return children
        elif node.t == "heading":
            return Paragraph(self.cmtohtml(node), self.styles["Heading{}".format(node.level)])
        else:
            node.pretty()
            raise RuntimeError("Unknown node type: {}".format(node.t))

    def render(self, cmtext):
        ast = self.cmparser.parse(cmtext)
        assert(ast.t == "document")
        return [self.cmnode_to_flowable(node) for node in self.AstSeq(ast)]

