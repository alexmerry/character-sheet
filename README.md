This is a script to generate a D&D 5e character sheet in a design I like better than the standard
designs.

Images (unaltered) from [Game-Icons.net](http://game-icons.net/) under a
[CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/) licence, by the following authors:

* [Delapouite](http://delapouite.com/) (high-kick.svg, jump-across.svg, shoulder-armor.svg,
  weight-lifting-up.svg))
* [Willdabeast](http://wjbstories.blogspot.com/) (orb-wand.svg)
* [Lorc](http://lorcblog.blogspot.co.uk/) (everything else)


TODO
====

* Think up a better name
* better presentation of hit points
* weapons?
* free text

